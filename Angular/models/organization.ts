import {IPhoto} from "./photo";
import {ITag} from "./tag";
import {IEvent, IEventP} from "./event";
import {IKeyword} from "./keyword";

export interface IOrganization {
  id?: number;
  title: string;
  isActive?: boolean;
  description: string;
  handle: string;
  feedbackEmail?: string;
  currentHandle?: string;
  address: string;
  lat?: number;
  lng?: number;
  logo?: IPhoto;
  previews?: IPhoto[];
  pastEvents?: IEvent[];
  nextEvents?: IEvent[];
  keywords?: IKeyword[];
  tags?: ITag[];
  privateTags?: ITag[];
  allowOrganization?: ITag[];
  trueRating?: number;
  modifiableRating?: number;
  website?: string;
  email?: string;
  phoneNumber?: string;
  facebook?: string;
}

export interface IOrganizationP {
  rows: IOrganization[];
  count: number;
}
