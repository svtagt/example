import {Injectable} from '@Angular/core';
import {createSelector, Selector} from 'reselect';
import {IOrganizationState} from '../reducers/organization.reducer';
import {RootState} from '../reducers';
import {IOrganization, IOrganizationP} from "../models/organization";
import {IEvent} from "../models/event";
import {ITag} from "../models/tag";
import { IRating } from "../models/rating";

@Injectable()
export class OrganizationSelector {
  constructor() {
  }

  getByStore(state: RootState) {
    return state.organization;
  }

  getList(): Selector<RootState, IOrganizationP>  {
    return createSelector(this.getByStore, (state: IOrganizationState) => {
      return state.list;
    });
  }

  getExtended(): Selector<RootState, IOrganization> {
    return createSelector(this.getByStore, (state: IOrganizationState) => {
      return state.current
    });
  }

  getEvents(type: string): Selector<RootState, IEvent[]> {
    return createSelector(this.getByStore, (state: IOrganizationState) => state.current[type]);
  }

  getCurrentRatings(): Selector<RootState, IRating[]> {
    return createSelector(this.getByStore, (state: IOrganizationState) => state.currentRatings);
  }

  getOrgPublicTags(): Selector<RootState, ITag[]> {
    return createSelector(this.getByStore, (state: IOrganizationState) => state.current.tags);
  }

  getOrgPrivateTags(): Selector<RootState, ITag[]> {
    return createSelector(this.getByStore, (state: IOrganizationState) => state.current.privateTags);
  }

  getOrgDonateTags(): Selector<RootState, ITag[]> {
    return createSelector(this.getByStore, (state: IOrganizationState) => state.current.allowOrganization);
  }

}
