import {Action} from '@ngrx/store';
import {IOrganization, IOrganizationP} from '../models/organization';
import {
  INIT_LIST,
  SET_CURRENT,
  PUSH_ORG_TO_LIST, MOVE_ORG_TO_TRASH_BIN, ORG_ADD_KEYWORD, ORG_DELETE_KEYWORD, ORG_DELETE_PREVIEW,
  ORG_DELETE_PUBLIC_TAG, ORG_ADD_PUBLIC_TAG, ORG_ADD_PREVIEW,
  ORG_SET_AVATAR, ORG_SET_ACTIVE_STATUS, ORG_SET_CURRENT_RATINGS, ORG_ADD_PRIVATE_TAG, ORG_DELETE_PRIVATE_TAG,
  SET_ALLOW_PRIVATE_TAGS, ADD_ALLOW_PRIVATE_TAGS, DELETE_ALLOW_PRIVATE_TAGS
} from '../actions/organization.action';
import { IRating } from "../models/rating";

export interface IOrganizationState {
  list: IOrganizationP,
  current: IOrganization;
  currentRatings: IRating[];
}

export const initialState: IOrganizationState = {
  list: {
    rows: [],
    count: -1
  },
  current: null,
  currentRatings: []
};

export function organizationReducer(state: IOrganizationState = initialState, {type, payload}: Action) {
  switch (type) {
    case INIT_LIST:
      return {...state, list: payload};
    case SET_CURRENT:
      return {
        ...state,
        current: {
          ...state.current,
          ...payload
        }
      };
    case ORG_SET_CURRENT_RATINGS:
      return {...state, currentRatings: payload};
    case PUSH_ORG_TO_LIST:
      return {
        ...state,
        list: {
          ...state.list,
          rows: [...state.list.rows, payload],
          count: state.list.count + 1
        }
      };
    case MOVE_ORG_TO_TRASH_BIN:
      return {
        ...state,
        list: {
          ...state.list,
          rows: state.list.rows.filter(org => org.handle !== payload),
          count: state.list.count - 1
        }
      };
    case ORG_DELETE_KEYWORD:
      return {
        ...state, current: {
          ...state.current,
          keywords: state.current.keywords.filter(ticket => ticket.id !== payload),
        }
      };
    case ORG_DELETE_PREVIEW:
      return {
        ...state, current: {
          ...state.current,
          previews: state.current.previews.filter(preview => preview.handle !== payload),
        }
      };
    case ORG_ADD_KEYWORD:
      return {
        ...state, current: {
          ...state.current,
          keywords: [...state.current.keywords, payload]
        }
      };
    case ORG_DELETE_PUBLIC_TAG:
      return {
        ...state, current: {
          ...state.current,
          tags: state.current.tags.filter(keyword => keyword.id !== payload),
        }
      };
    case ORG_DELETE_PRIVATE_TAG:
      return {
        ...state, current: {
          ...state.current,
          privateTags: state.current.privateTags.filter(keyword => keyword.id !== payload),
        }
      };
    case ORG_ADD_PUBLIC_TAG:
      return {
        ...state, current: {
          ...state.current,
          tags: [payload, ...state.current.tags]
        }
      };
    case ORG_ADD_PRIVATE_TAG:
      return {
        ...state, current: {
          ...state.current,
          privateTags: [payload, ...state.current.privateTags]
        }
      };
    case ORG_ADD_PREVIEW:
      return {
        ...state,
        current: {
          ...state.current,
          previews: [
            ...state.current.previews,
            payload
          ]
        }
      };
    case ORG_SET_AVATAR:
      return {
        ...state,
        current: {
          ...state.current,
          logo: payload
        }
      };
    case ORG_SET_ACTIVE_STATUS:
      return {
        ...state,
        current: {
          ...state.current,
          isActive: payload
        }
      };

    case SET_ALLOW_PRIVATE_TAGS:
      return {
        ...state,
        current: {
          ...state.current,
          allowOrganization: payload
        }
      };
    case ADD_ALLOW_PRIVATE_TAGS:
      return {
        ...state,
        current: {
          ...state.current,
          allowOrganization: [...state.current.allowOrganization, payload]
        }
      };
    case DELETE_ALLOW_PRIVATE_TAGS:
      return {
        ...state,
        current: {
          ...state.current,
          allowOrganization: state.current.allowOrganization.filter(tag => {
            return tag.id !== payload;
          }),
        }
      };
    default:
      return state;
  }
}