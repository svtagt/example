import { Action } from '@ngrx/store';
import {IOrganization, IOrganizationP} from '../models/organization';
import {ORGANIZATION} from "../const";
import {IKeyword} from "../models/keyword";
import {ITag} from "../models/tag";
import {IPhoto} from '../models/photo';
import { IRating } from "../models/rating";

export const INIT_LIST                    = `${ORGANIZATION} Init list`;
export const PUSH_ORG_TO_LIST             = `${ORGANIZATION} Push to list`;
export const SET_CURRENT                  = `${ORGANIZATION} Set current`;
export const ORG_SET_CURRENT_RATINGS      = `${ORGANIZATION} Set current ratings`;
export const MOVE_ORG_TO_TRASH_BIN        = `${ORGANIZATION} move to trash bin`;
export const ORG_DELETE_KEYWORD           = `${ORGANIZATION} Delete keyword`;
export const ORG_DELETE_PREVIEW           = `${ORGANIZATION} Delete preview`;
export const ORG_ADD_PREVIEW              = `${ORGANIZATION} Add preview`;
export const ORG_ADD_KEYWORD              = `${ORGANIZATION} Add keyword`;
export const ORG_ADD_PUBLIC_TAG           = `${ORGANIZATION} Add public tag`;
export const ORG_ADD_PRIVATE_TAG          = `${ORGANIZATION} Add private tag`;
export const ORG_DELETE_PUBLIC_TAG        = `${ORGANIZATION} Add delete public tag`;
export const ORG_DELETE_PRIVATE_TAG       = `${ORGANIZATION} Add delete private tag`;
export const ORG_SET_AVATAR               = `${ORGANIZATION} Set avatar`;
export const ORG_SET_ACTIVE_STATUS        = `${ORGANIZATION} Set active status`;
export const SET_ALLOW_PRIVATE_TAGS       = `${ORGANIZATION} Set allow private tags`;
export const ADD_ALLOW_PRIVATE_TAGS       = `${ORGANIZATION} add allow private tags`;
export const DELETE_ALLOW_PRIVATE_TAGS    = `${ORGANIZATION} delete allow private tags`;


export class orgSetActiveStatus implements Action{
  readonly type = ORG_SET_ACTIVE_STATUS;
  constructor(public payload: boolean) {
  }
}

export class OrganizationSetList implements Action {
  readonly type = INIT_LIST;
  constructor(public payload: IOrganizationP) {
  }
}

export class PushToList implements Action {
  readonly type = PUSH_ORG_TO_LIST;
  constructor(public payload: IOrganization) {
  }
}

export class SetCurrent implements Action {
  readonly type = SET_CURRENT;
  constructor(public payload: IOrganization) {
  }
}

export class orgSetCurrentRatings implements Action {
  readonly type = ORG_SET_CURRENT_RATINGS;
  constructor(public payload: IRating[]) {
  }
}

export class moveOrgToTrashBin implements Action {
  readonly type = MOVE_ORG_TO_TRASH_BIN;
  constructor(public payload: string) {
  }
}

export class orgDeleteKeyword implements Action{
  readonly type = ORG_DELETE_KEYWORD;
  constructor(public payload: number) {
  }
}

export class orgDeletePreview implements Action{
  readonly type = ORG_DELETE_PREVIEW;
  constructor(public payload: string) {
  }
}

export class orgAddKeyword implements Action{
  readonly type = ORG_ADD_KEYWORD;
  constructor(public payload: IKeyword) {
  }
}

export class orgAddPublicTag implements Action{
  readonly type = ORG_ADD_PUBLIC_TAG;
  constructor(public payload: ITag) {
  }
}

export class orgAddPrivateTag implements Action{
  readonly type = ORG_ADD_PRIVATE_TAG;
  constructor(public payload: ITag) {
  }
}

export class orgDeletePublicTag implements Action{
  readonly type = ORG_DELETE_PUBLIC_TAG;
  constructor(public payload: number) {
  }
}

export class orgDeletePrivateTag implements Action{
  readonly type = ORG_DELETE_PRIVATE_TAG;
  constructor(public payload: number) {
  }
}

export class orgSetAvatar implements Action {
  readonly type = ORG_SET_AVATAR;
  constructor(public payload: IPhoto) {}
}

export class orgAddPreview implements Action {
  readonly type = ORG_ADD_PREVIEW;
  constructor(public payload: IPhoto) {}
}

export class setAllowPrivateTags implements Action {
  readonly type = SET_ALLOW_PRIVATE_TAGS;
  constructor(public payload: ITag[]) {}
}

export class addAllowPrivateTags implements Action {
  readonly type = ADD_ALLOW_PRIVATE_TAGS;
  constructor(public payload: ITag) {}
}

export class deleteAllowPrivateTags implements Action {
  readonly type = DELETE_ALLOW_PRIVATE_TAGS;
  constructor(public payload: number) {}
}
