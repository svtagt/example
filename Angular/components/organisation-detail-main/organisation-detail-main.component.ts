import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {IOrganisation, IOrganisationAddit} from '../../../models/organisation';
import {ModalService} from '../../../shared/services/modal.service';
import {OrganisationService} from '../../../shared/services/organisation.service';

@Component({
  selector: 'd-organisation-detail-main',
  templateUrl: './organisation-detail-main.component.html',
  styleUrls: ['./organisation-detail-main.component.sass']
})
export class OrganisationDetailMainComponent implements OnInit {
  @Input() organisation$: Observable<IOrganisationAddit>;
  organisation: IOrganisationAddit;

  constructor(
    private _modalService: ModalService,
    private _organisationService: OrganisationService
  ) {}

  ngOnInit() {
    this.organisation$
      .subscribe((_organisation: IOrganisationAddit) => this.organisation = _organisation);
  }

  onToggleFavorite(isFavorited: boolean) {
    this._modalService.openNotActive(null, () => this._organisationService.toggleFavorite(isFavorited, this.organisation.handle))
      .subscribe();
  }
}