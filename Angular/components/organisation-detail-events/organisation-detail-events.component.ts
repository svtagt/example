import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {IEventP} from '../../../models/event';

@Component({
  selector: 'd-organisation-detail-events',
  templateUrl: './organisation-detail-events.component.html',
  styleUrls: ['./organisation-detail-events.component.sass']
})
export class OrganisationDetailEventsComponent implements OnInit {
  @Input() pastEvents$: Observable<IEventP>;
  @Input() nextEvents$: Observable<IEventP>;

  constructor() {}

  ngOnInit() {}
}