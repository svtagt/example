import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {IAlbum, IAlbumPhotoP} from '../../../models/album';
import {AlbumService} from '../../../shared/services/album.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {UiService} from '../../../shared/services/ui.service';
import {IUserStateAuth} from '../../../models/user';
import {UserService} from '../../../shared/services/user.service';
import {ModalService} from '../../../shared/services/modal.service';

@Component({
  selector: 'd-album-detail',
  templateUrl: './album-detail.page.html',
  styleUrls: ['./album-detail.page.sass']
})
export class AlbumDetailPage implements OnInit, OnDestroy {
  album$: Observable<IAlbumPhotoP>;
  album: IAlbumPhotoP;
  handle: string;
  userAuth: IUserStateAuth;

  hidden: boolean = false;
  isOpen: boolean = false;

  constructor(
    private _albumService: AlbumService,
    private _userService: UserService,
    private _uiService: UiService,
    private _modalService: ModalService,
    private _aRoute: ActivatedRoute,
    private _router: Router,
    private _location: Location
  ) {
    //this.handle = this._aRoute.snapshot.params.handle;
    this.album$ = this._albumService.getDetailS();
    this.album$
      .subscribe((_album: IAlbumPhotoP) => this.album = _album);

    this._userService.getStateAuth()
      .subscribe((_userAuth: IUserStateAuth) => {
        this.userAuth = _userAuth;
      });
  }

  ngOnInit() {
    this._uiService.getModalOpenByType('album')
      .subscribe((_isOpen: boolean) => {
        this.isOpen = _isOpen;
        if (_isOpen) {
          this._albumService.initDetail(this.handle);
        } else {
          this._albumService.destroyDetail();
        }
      });
    this._aRoute.queryParams
      //.filter(params => params['album_detail'])
      .subscribe(params => {
        this.handle = params['album_detail'];
        if (this.handle) {
          this._uiService.toggleModalOpenByType('album', true);
        } else {
          this._uiService.toggleModalOpenByType('album', false);
        }
      });
    //this._uiService.toggleModalOpenByType('album', true);
    this._uiService.getModalOpenByType('photo')
      .subscribe((_hidden: boolean) => this.hidden = _hidden);
  }

  ngOnDestroy() {
    this._uiService.toggleModalOpenByType('album', false);
  }

  onClose() {
    this._location.back();
  }

  openPhoto(photoId: number) {
    this._uiService.openSliderModal(
      this.album.photos.rows,
      photoId,
      {
        'photo_type': 'album',
        'photo_album-handle': this.album.handle,
        'photo_handle': this.album.photos.rows[photoId].handle
      }
    );
  }

  onToggleFavorite(isFavorite: boolean) {
    this._modalService.openNotActive(null, this._albumService.toggleFavorite.bind(this._albumService, this.handle, isFavorite))
      .subscribe();
  }

  onToggleLike(isLiked: boolean, handle: string) {
    this._albumService.toggleLike(isLiked, handle);
  }
}