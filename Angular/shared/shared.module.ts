import { NgModule } from '@Angular/core';
import { HttpModule } from '@Angular/http';

import { ApiService } from './services/requests/api.service';
import { AuthService } from './services/requests/auth.service';
import { OrganizationRequestService } from './services/requests/organization.service';
import { OrganizationService } from './services/organization.service';
import { EventRequestService } from "./services/requests/event.service";
import { EventService } from "./services/event.service";
import { AlbumRequestService } from './services/requests/album.service';
import { AlbumService } from './services/album.service';
import { DFileUploadService } from "./services/file-upload.service";
import { PhotoRequestService } from "./services/requests/photo.service";
import {GuestRequestService} from "./services/requests/guest.service";
import {GuestService} from "./services/guest.service";
import {BinService} from "./services/bin.service";
import {BinRequestService} from "./services/requests/bin.service";
import { KeywordService } from "./services/keyword.service";
import { KeywordRequestService } from "./services/requests/keyword.service";
import { PhotoService } from "./services/photo.service";
import { TagRequestService } from "./services/requests/tag.service";
import { TagService } from "./services/tag.service";
import { AnalyticsService } from "./services/analytics.service";
import { AnalyticsRequestService } from "./services/requests/analytics.service";
import {FeatureService} from "./services/feature.service";
import {FeatureRequestService} from "./services/requests/feature.service";
import { AdministrationRequestService } from "./services/requests/administration.service";
import { AdministrationService } from "./services/administration.service";
import { DataService } from "./services/data.service";
import {ManagementReqService} from "./services/requests/management.service";
import {ManagementService} from "./services/management.service";
import {LogService} from "./services/log.service";
import {LogRequestService} from "./services/requests/log.service";
import {LocationService} from './services/location.service'
import { GeocodeRequestService } from "./services/requests/geocode.service";
import {ConfigService} from './services/config.service';
import {TicketRequestService} from './services/requests/ticket.service';
import {PromocodeRequestService} from './services/requests/promocode.service';
import {PromocodeService} from './services/promocode.service';
import {TaxRequestService} from './services/requests/tax.service';
import {TaxService} from './services/tax.service';
import {TicketCategoryService} from './services/ticket-category.service';
import {HelperService} from './services/helper.service';
import {TicketService} from './services/ticket.service';

const services = [
  ApiService,
  AuthService,
  OrganizationRequestService,
  EventRequestService,
  EventService,
  OrganizationService,
  GuestRequestService,
  GuestService,
  AlbumRequestService,
  AlbumService,
  BinService,
  BinRequestService,
  DFileUploadService,
  PhotoService,
  PhotoRequestService,
  KeywordService,
  KeywordRequestService,
  TagService,
  TagRequestService,
  AnalyticsService,
  AnalyticsRequestService,
  FeatureService,
  FeatureRequestService,
  AdministrationService,
  AdministrationRequestService,
  DataService,
  ManagementService,
  ManagementReqService,
  LogService,
  LogRequestService,
  LocationService,
  GeocodeRequestService,
  ConfigService,
  TicketRequestService,
  PromocodeRequestService,
  PromocodeService,
  TaxRequestService,
  TaxService,
  TicketCategoryService,
  HelperService,
  TicketService
];

const modules = [
  HttpModule
];

@NgModule({
  imports: [
    ...modules
  ],
  declarations: [
    // ...components
  ],
  providers: [
    ...services
  ],
  exports: [
    ...modules,
    // ...components
  ]
})
export class SharedModule {
}
