import {Injectable} from '@Angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import {IOrganization, IOrganizationP} from '../../models/organization';
import {RootState} from '../../reducers';
import {
  OrganizationSetList,
  PushToList,
  SetCurrent,
  moveOrgToTrashBin,
  orgAddPublicTag,
  orgDeletePublicTag,
  orgSetAvatar,
  orgAddPreview, orgSetActiveStatus, orgSetCurrentRatings, orgDeletePrivateTag, orgAddPrivateTag, setAllowPrivateTags,
  addAllowPrivateTags, deleteAllowPrivateTags
} from '../../actions/organization.action';

import {OrganizationRequestService} from './requests/organization.service';
import {IEvent, IEventP} from "../../models/event";
import {OrganizationSelector} from "../../selectors/organization.selector";
import {putOrgToTrashBin} from "../../actions/bin.action";
import {ITag} from "../../models/tag";
import {IPhoto} from '../../models/photo';
import { IRating } from "../../models/rating";

@Injectable()
export class OrganizationService {
  constructor(private _orgReqService: OrganizationRequestService,
              private _orgSelector: OrganizationSelector,
              private _store: Store<RootState>) {
  }

  initList(page: number, limit: number) {
    this._orgReqService.getList(page, limit)
      .subscribe((organizations: IOrganizationP) => {
        this._store.dispatch(new OrganizationSetList(organizations));
      });
  }

  getExtendedS(): Observable<IOrganization> {
    return this._store.select(this._orgSelector.getExtended());
  }

  initCurrent(handle: string) {
    this._orgReqService.getByHandle(handle)
      .subscribe((organization: IOrganization) => {
        this._store.dispatch(new SetCurrent(organization));
      });
  }

  initCurrentRatings(handle: string) {
    this._orgReqService.getCurrentRatings(handle)
      .subscribe((ratings: IRating[]) => {
        this._store.dispatch(new orgSetCurrentRatings(ratings));
      });
  }

  getCurrentRatings(): Observable<IRating[]> {
    return this._store.select(this._orgSelector.getCurrentRatings());
  }

  create(data: IOrganization): Observable<IOrganization> {
    return this._orgReqService.create(data)
      .map((organization: IOrganization) => {
        this._store.dispatch(new PushToList(organization));
        return organization;
      });
  }

  editByHandle(data: IOrganization): Observable<IOrganization> {
    return this._orgReqService.editByHandle(data.currentHandle, data)
      .map((organization: IOrganization) => {
        this._store.dispatch(new SetCurrent(organization));
        return organization;
      });
  }

  moveToTrashBin(handle: string): Observable<IOrganization> {
    return this._orgReqService.moveToTrashBin(handle)
      .map((organization: IOrganization) => {
        this._store.dispatch(new moveOrgToTrashBin(handle));
        this._store.dispatch(new putOrgToTrashBin(organization));
        return organization;
      });
  }

  getEventsS(type: string): Observable<IEvent[]> {
    return this._store.select(this._orgSelector.getEvents(type));
  }

  addPublicTag(orgHandle: string, tagId: number): Observable<ITag> {
    return this._orgReqService.addPublicTag(orgHandle, tagId)
      .map((tag: ITag) => {
        this._store.dispatch(new orgAddPublicTag(tag));
        return tag;
      })
  }

  addPrivateTag(orgHandle: string, tagId: number): Observable<ITag> {
    return this._orgReqService.addPrivateTag(orgHandle, tagId)
      .map((tag: ITag) => {
        this._store.dispatch(new orgAddPrivateTag(tag));
        return tag;
      })
  }

  deletePublicTag(orgHandle: string, tagId: number): Observable<ITag> {
    return this._orgReqService.deletePublicTag(orgHandle, tagId)
      .map((tag: ITag) => {
        this._store.dispatch(new orgDeletePublicTag(tagId));
        return tag;
      })
  }

  deletePrivateTag(orgHandle: string, tagId: number): Observable<ITag> {
    return this._orgReqService.deletePrivateTag(orgHandle, tagId)
      .map((tag: ITag) => {
        this._store.dispatch(new orgDeletePrivateTag(tagId));
        return tag;
      })
  }

  setAvatar(avatar: IPhoto): void {
    this._store.dispatch(new orgSetAvatar(avatar));
  }

  setActiveStatus(handle: string, status: boolean): Observable<IOrganization> {
    return this._orgReqService.setActiveStatus(handle, status)
      .map((organization: IOrganization) => {
        this._store.dispatch(new orgSetActiveStatus(status));
        return organization;
      });
  }

  addPreview(preview: IPhoto): void {
    this._store.dispatch(new orgAddPreview(preview));
  }

  getAllowPrivateTags(handle: string): Observable<ITag[]> {
    return this._orgReqService.getAllowPrivateTags(handle)
      .map((tags: ITag[]) => {
        this._store.dispatch(new setAllowPrivateTags(tags));
        return tags;
      });
  }

  addAllowPrivateTags(handle: string, tagId: number): Observable<ITag> {
    return this._orgReqService.addAllowPrivateTags(handle, tagId)
      .map((tag: ITag) => {
        this._store.dispatch(new addAllowPrivateTags(tag));
        return tag;
      });
  }

  deleteAllowPrivateTags(handle: string, tagId: number): Observable<ITag> {
    return this._orgReqService.deleteAllowPrivateTags(handle, tagId)
      .map((tag: ITag) => {
        this._store.dispatch(new deleteAllowPrivateTags(tag.id));
        return tag;
      });
  }

}
