import { Injectable } from '@Angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { ApiService } from './api.service';

@Injectable()
export class AuthService {
  private _token: string = '';
  private _user: any = null;
  private _session: any = null;

  constructor(private _apiService: ApiService) {
  }

  private setUser(user: any, session: any) {
    this.token = session && session.token ? session.token : '';
    this.user = user;
    this.session = session;
  }

  login(email: string, password: string): Observable<any> {
    return this._apiService.post('/user/login', {body: {email, password}})
      .map(data => {
        this.setUser(data.user, data.session);
        return data;
      });
  }

  loginByToken(): Observable<any> {
    return this._apiService.get('/user')
      .map(data => {
        this.setUser(data.user, data.session);
        return data;
      });
  }

  logout() {
    this.setUser(null, null);
  }

  relogin() {
    if (this.checkToken()) {
      return this.loginByToken()
        .catch(rej => {
          this.logout();
          throw rej;
        });
    }
    return Observable.of(false);
  }

  checkToken() {
    let token = this.token;
    if (token && token.length) {
      this._apiService.setToken(token);
      return true;
    }
    return false;
  }

  public get token(): string {
    return this._token || localStorage.getItem('accessToken');
  }

  public set token(token: string) {
    if (token && token.length) {
      localStorage.setItem('accessToken', token);
    } else {
      localStorage.removeItem('accessToken');
    }
    this._apiService.setToken(token);
    this._token = token;
  }

  public get user() {
    return this._user;
  }

  public set user(user) {
    this._user = user;
  }

  public get session() {
    return this._session;
  }

  public set session(session) {
    this._session = session;
  }
}
