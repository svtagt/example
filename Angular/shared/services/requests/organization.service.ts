import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import {IOrganization, IOrganizationP} from '../../../models/organization';
import { ApiService } from './api.service';
import { IEventP } from "../../../models/event";
import { IPhotoP } from "../../../models/photo";
import {ITag} from "../../../models/tag";
import { IRating } from "../../../models/rating";

@Injectable()
export class OrganizationRequestService {
  constructor(private _apiService: ApiService) {
  }

  getList(page: number,limit: number): Observable<IOrganizationP> {
    return this._apiService.get(`/organization?page=${page}&limit=${limit}`);
  }

  getByHandle(handle: string): Observable<IOrganization> {
    return this._apiService.get(`/organization/${handle}`);
  }

  getCurrentRatings(handle: string): Observable<IRating[]> {
    return this._apiService.get(`/analytics/ratings/${handle}?type=1`);
  }

  getEventsByHandle(handle: string): Observable<IEventP> {
    return this._apiService.get(`/organization/event/${handle}`);
  }

  getPreviewsByHandle(handle: string): Observable<IPhotoP> {
    return this._apiService.get(`/organization/previews/${handle}`);
  }

  create(data: IOrganization): Observable<IOrganization> {
    return this._apiService.post('/organization', {body: data});
  }

  editByHandle(handle: string, data: IOrganization): Observable<IOrganization> {
    return this._apiService.put(`/organization/${handle}`, {body: data});
  }

  moveToTrashBin(handle: string): Observable<IOrganization> {
    return this._apiService.put(`/organization/trash/${handle}`, {
      body: {
        removed: true
      }
    });
  }

  addPublicTag(orgHandle: string, tagId: number): Observable<ITag> {
    return this._apiService.put(`/organization/tag/${orgHandle}`, {
      body: {
        tagId: tagId
      }
    });
  }

  addPrivateTag(orgHandle: string, tagId: number): Observable<ITag> {
    return this._apiService.put(`/organization/private/${orgHandle}`, {
      body: {
        tagId: tagId
      }
    });
  }

  deletePublicTag(orgHandle: string, tagId: number): Observable<ITag> {
    return this._apiService.delete(`/organization/tag/${orgHandle},${tagId}`);
  }

  deletePrivateTag(orgHandle: string, tagId: number): Observable<ITag> {
    return this._apiService.delete(`/organization/private/${orgHandle},${tagId}`);
  }

  setActiveStatus(handle: string, status: boolean): Observable<IOrganization> {
    return this._apiService.put(`/organization/active/${handle}`, {
      body: {
        active: status
      }
    });
  }

  getAllowPrivateTags(handle: string): Observable<ITag[]> {
    return this._apiService.get(`/organization/allow/${handle}`);
  }

  addAllowPrivateTags(handle: string, tagId: number): Observable<ITag> {
    return this._apiService.put(`/organization/allow/${handle}`, {
      body: {
        tagId: tagId
      }
    });
  }

  deleteAllowPrivateTags(handle: string, tagId: number): Observable<ITag> {
    return this._apiService.delete(`/organization/allow/${handle},${tagId}`);
  }

}