import { Injectable } from '@Angular/core';
import { Http, Response, Headers, RequestOptions, RequestOptionsArgs, BaseRequestOptions } from '@Angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {HOST} from "../../../const";
import {ConfigService} from '../config.service';

@Injectable()
export class ApiService extends BaseRequestOptions {
  constructor(
    private _http: Http,
    private _configService: ConfigService
  ) {
    super();
    this.headers.append('Content-Type', 'application/json');
  }

  private call(url: string, params: RequestOptionsArgs): Observable<any> {
    let requestOptions = this.merge(params);
    return this._http.request(`${this._configService.config.apiUrl}${this._configService.config.prefixUrl}${url}`, requestOptions)
      .map(res => {
        let _res = res.json();
        if (_res.success) {
          return _res.data;
        } else {
          throw _res.error;
        }
      })
      .catch(rej => {
        throw rej;
      });
  }

  post(url: string, params: RequestOptionsArgs = new RequestOptions()) {
    let paramData = Object.assign(params, {method: 'post'});
    return this.call(url, paramData);
  }

  get(url: string, params: RequestOptionsArgs = new RequestOptions()) {
    let paramData = Object.assign(params, {method: 'get'});
    return this.call(url, paramData);
  }

  pureGet(url: string, params: RequestOptionsArgs = new RequestOptions()) {
    let paramData = Object.assign(params, {method: 'get'});
    return this.pureCall(url, paramData);
  }

  put(url: string, params: RequestOptionsArgs = new RequestOptions()) {
    let paramData = Object.assign(params, {method: 'put'});
    return this.call(url, paramData);
  }

  delete(url: string, params: RequestOptionsArgs = new RequestOptions()) {
    let paramData = Object.assign(params, {method: 'delete'});
    return this.call(url, paramData);
  }

  setToken(token: string) {
    this.headers.set('x-token', token);
  }
}
