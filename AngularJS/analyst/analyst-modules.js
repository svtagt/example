'use strict';

// import appConfig from '../config';
import config from './analyst-routes';
import analystInitCtrl from './analyst-init/analyst-init-ctrl';
import analystHeaderCtrl from './analyst-header/analyst-header-ctrl';
import analystNavigationCtrl from './analyst-navigation/analyst-navigation-ctrl';

import analystWorkspaceCtrl from './analyst-workspace/analyst-workspace-ctrl';
import analystEntriesIndexCtrl from './analyst-entries/analyst-entries-index-ctrl';
import analystEntriesConsentsCtrl from './analyst-entries/consents/analyst-entries-consents-ctrl';
import analystEntriesEntriesCtrl from './analyst-entries/entries/analyst-entries-ctrl';
import analystEntriesOffersCtrl from './analyst-entries/offers/analyst-entries-offers-ctrl';
import analystEntriesTrashBinCtrl from './analyst-entries/trash-bin/analyst-entries-trash-bin-ctrl';
import analystProfileSettingsCtrl from './analyst-profile-settings/analyst-profile-settings-ctrl';
import analystFaqCtrl from './analyst-faq/analyst-faq-ctrl';

import analystCompareIndexCtrl from './analyst-compare/analyst-compare-index-ctrl';
import analystCompareSelectionCtrl from './analyst-compare/selection/analyst-compare-selection-ctrl';
import analystCompareCompareCtrl from './analyst-compare/compare/analyst-compare-compare-ctrl';


export default angular.module('analyst', [])
  // .constant('appConfig', appConfig)
  .config(config)
  .controller('analystInitCtrl', analystInitCtrl)
  .controller('analystHeaderCtrl', analystHeaderCtrl)
  .controller('analystNavigationCtrl', analystNavigationCtrl)

  .controller('analystWorkspaceCtrl', analystWorkspaceCtrl)
  .controller('analystEntriesIndexCtrl', analystEntriesIndexCtrl)
  .controller('analystEntriesConsentsCtrl', analystEntriesConsentsCtrl)
  .controller('analystEntriesEntriesCtrl', analystEntriesEntriesCtrl)
  .controller('analystEntriesOffersCtrl', analystEntriesOffersCtrl)
  .controller('analystEntriesTrashBinCtrl', analystEntriesTrashBinCtrl)
  .controller('analystProfileSettingsCtrl', analystProfileSettingsCtrl)
  .controller('analystFaqCtrl', analystFaqCtrl)

  .controller('analystCompareIndexCtrl', analystCompareIndexCtrl)
  .controller('analystCompareSelectionCtrl', analystCompareSelectionCtrl)
  .controller('analystCompareCompareCtrl', analystCompareCompareCtrl)
  .name;
