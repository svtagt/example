'use strict';

export default class analystEntriesCtrl {
  constructor(entryService, dictionaryService, modalService) {
    Object.assign(this, {
      entryService,
      dictionaryService,
      modalService,

      dictionaries: {
        currencies: [],
        provisions: [],
        entryStatuses: []
      },
      data: {},
      selectedToDelete: []
    });

    this.prepareView();
  }

  getList() {
    this.entryService.list()
      .then((res) => {
        for (let i = 0; i < res.data.length; i++) {
          res.data[i].currency = this.dictionaryService.getNameById(this.dictionaries.currencies, res.data[i].currencyId);
          res.data[i].provision = this.dictionaryService.getNameById(this.dictionaries.provisions, res.data[i].provisionId);
          res.data[i].entryStatus = this.dictionaryService.getNameById(this.dictionaries.entryStatuses, res.data[i].entryStatusId);
        }
        this.data = res.data;
        this.loaded = true;
        this.selectedAll = this.isSelectedAll();
      });
  }

  prepareDictionaries() {
    return this.dictionaryService.currencies()
      .then((res) => {
        this.dictionaries.currencies = res.data;
        return this.dictionaryService.provisions()
          .then((res1) => {
            this.dictionaries.provisions = res1.data;
            return this.dictionaryService.entryStatuses()
              .then((res2) => {
                this.dictionaries.entryStatuses = res2.data;
              });
          });
      });
  }

  prepareView() {
    this.prepareDictionaries()
      .then(() => {
        this.getList();
      });
  }

  showFiles(item) {
    //crutch
    this.selectRow(item);
    this.filesForShow = item.entryFiles;
    let ctrlScope = this;
    this.modalService.open({
      templateUrl: 'html/common/modals/files-modal.html',
      controller: function($uibModalInstance, appConfig, appConst) {
        this.files = ctrlScope.filesForShow;
        for (let i = 0; i < this.files.length; i++) {
          this.files[i].downloadLink = `${appConfig.api}${appConst.rest.appDelegates.entries}/download/${this.files[i].id}`;
        }
      },
      controllerAs: '$ctrl'
    });
  }

  uploadOfferFiles(id, files) {
    let ctrlScope = this;
    this.modalService.open({
      templateUrl: 'html/common/modals/upload-files-modal.html',
      controller: function($uibModalInstance, offerService) {
        let currentUpload = offerService.uploadFiles(id, files);
        currentUpload
          .then((resp) => {
            if (resp.data.error_code === 0) {
              $uibModalInstance.close();
              ctrlScope.establishAfterCreate();
            } else {
              // alert('an error occured');
            }
          }, (resp) => { //catch error
            // alert('Error status: ' + resp.status);
          }, (evt) => {
            this.progress = parseInt(100.0 * evt.loaded / evt.total);
          });
        this.cancel = function() {
          currentUpload.abort();
          $uibModalInstance.close();
        };
      },
      controllerAs: '$ctrl'
    });
  }

  establishAfterCreate() {
    this.getList();
    this.modalService.open({
      templateUrl: 'html/common/modals/offer-uploaded-modal.html'
    });
  }


  tryOffer() {
    let ctrlScope = this;
    this.modalService.open({
      templateUrl: 'html/common/modals/new-offer-modal.html',
      controllerAs: '$ctrl',
      controller: function($uibModalInstance, offerService, appliedService) {
        this.currencies = appliedService.makeSelectList(ctrlScope.dictionaries.currencies, 'id', 'name');
        this.newOffer = {
          entries: [],
          currencyId: appliedService.selectFirstPropertyName(this.currencies),
          periodType: 1
        };

        this.prepareSelectedEntries = function() {
          for (let i = 0; i < ctrlScope.data.length; i++) {
            if (ctrlScope.data[i].$selected) {
              this.newOffer.entries.push(ctrlScope.data[i].id);
            }
          }
        };

        this.removeFileFromList = function(index) {
          this.newOffer.files.splice(index, 1);
        };

        this.makeOffer = function() {
          let offer = angular.copy(this.newOffer);
          if (offer.periodType === '2') {
            offer.period = parseInt(offer.period) * 12;
          }
          offer.interestRate = offer.interestRate.substring(0, offer.interestRate.indexOf('%') - 1);
          offerService.add(offer)
            .then((res) => {
              this.uploadOfferFiles(res.data.id);
            });
        };

        this.uploadOfferFiles = function(id) {
          $uibModalInstance.close();
          ctrlScope.uploadOfferFiles(id, this.newOffer.files)
        };

        this.prepareInterestRate = function(kind) {
          if (this.newOffer.interestRate) {
            if (kind === true) {
              this.newOffer.interestRate += ' %'
            } else {
              this.newOffer.interestRate = this.newOffer.interestRate.substring(0, this.newOffer.interestRate.indexOf('%') - 1)
            }
          }
        };

        //start
        this.prepareSelectedEntries();
      }
    });
  }

  selectRow(item) {
    item.$selected = !item.$selected;
    this.selectedAll = this.isSelectedAll();
  }

  selectAll() {
    if (this.selectedAll) {
      for (let i = 0; i < this.data.length; i++) {
        this.data[i].$selected = true;
      }
    } else {
      for (let i = 0; i < this.data.length; i++) {
        this.data[i].$selected = false;
      }
    }

  }

  countOfSelected() {
    let count = 0;
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].$selected) {
        count++;
      }
    }
    return count;
  }

  isSelectedAll() {
    return this.data.length > 0 && this.countOfSelected() === this.data.length;
  }
}
