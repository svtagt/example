'use strict';

export default class analystEntriesConsentsCtrl {
  constructor(offerService, dictionaryService, disputeService, modalService) {
    Object.assign(this, {
      offerService,
      dictionaryService,
      disputeService,
      modalService,

      dictionaries: {
        currencies: [],
        provisions: [],
        offerStatuses: []
      },
      data: {},
      selectedToDelete: []
    });
    this.prepareView();
  }

  prepareView() {
    this.prepareDictionaries()
      .then(() => {
        this.getList();
      });
  }

  prepareDictionaries() {
    return this.dictionaryService.currencies()
      .then((res) => {
        this.dictionaries.currencies = res.data;
        return this.dictionaryService.provisions()
          .then((res1) => {
            this.dictionaries.provisions = res1.data;
            return this.dictionaryService.offerStatuses()
              .then((res2) => {
                this.dictionaries.offerStatuses = res2.data;
              });
          });
      });
  }

  getList() {
    this.disputeService.listConsents()
      .then((res) => {
        for (let i = 0; i < res.data.length; i++) {
          if (res.data[i].offer) {
            res.data[i].offer.currency = this.dictionaryService.getNameById(this.dictionaries.currencies, res.data[i].offer.currencyId);
            res.data[i].offer.provision = this.dictionaryService.getNameById(this.dictionaries.provisions, res.data[i].offer.provisionId);
            res.data[i].offer.offerStatus = this.dictionaryService.getNameById(this.dictionaries.offerStatuses, res.data[i].offer.offerStatusId);
          }
        }
        this.data = res.data;
        this.loaded = true;
        this.selectedAll = this.isSelectedAll();
      });
  }

  showFiles(item) {
    //crutch
    this.selectRow(item);
    this.filesForShow = item.offer.offerFiles;
    let ctrlScope = this;
    this.modalService.open({
      templateUrl: 'html/common/modals/files-modal.html',
      controller: function($uibModalInstance, appConfig, appConst) {
        this.files = ctrlScope.filesForShow;
        for (let i = 0; i < this.files.length; i++) {
          this.files[i].downloadLink = `${appConfig.api}${appConst.rest.appDelegates.offers}/download/${this.files[i].id}`;
        }
      },
      controllerAs: '$ctrl'
    });
  }

  showContacts(item) {
    //crutch
    this.selectRow(item);
    let ctrlScope = this;
    this.modalService.open({
      templateUrl: 'html/common/modals/contacts-modal.html',
      controller: function($uibModalInstance, appConfig, appConst) {
        this.data = item.borrower;
      },
      controllerAs: '$ctrl'
    });
  }

  removeDisputes() {
    let ctrlScope = this;
    this.modalService.open({
      templateUrl: 'html/common/modals/confirm-delete-modal.html',
      controllerAs: '$ctrl',
      controller: function($uibModalInstance) {
        this.ok = function() {
          ctrlScope.confirmRemove()
            .then(() => {
              ctrlScope.getList();
              $uibModalInstance.close();
            });
        };
      }
    });
  }

  confirmRemove() {
    let idsForRemove = [];
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].$selected) {
        idsForRemove.push(this.data[i].id);
      }
    }
    return this.disputeService.remove({ids: idsForRemove});
  }

  selectRow(item) {
    item.$selected = !item.$selected;
    this.selectedAll = this.isSelectedAll();
  }

  selectAll() {
    if (this.selectedAll) {
      for (let i = 0; i < this.data.length; i++) {
        this.data[i].$selected = true;
      }
    } else {
      for (let i = 0; i < this.data.length; i++) {
        this.data[i].$selected = false;
      }
    }

  }

  countOfSelected() {
    let count = 0;
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].$selected) {
        count++;
      }
    }
    return count;
  }

  isSelectedAll() {
    return this.data.length > 0 && this.countOfSelected() === this.data.length;
  }
}
