'use strict';

function config($stateProvider, appConfig, appConst) {
  $stateProvider
    .state('analyst-init', {
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-init/analyst-init.html`,
      controller: 'analystInitCtrl',
      controllerAs: '$analystInit',
      abstract: true,
      params: {
        accessRole: appConst.roles.analyst
      }
    })
    .state('analyst', {
      parent: 'analyst-init',
      url: '/investor',
      views: {
        'header': {
          templateUrl: `${appConfig.tmpUrl}/analyst/analyst-header/analyst-header.html`,
          controller: 'analystHeaderCtrl',
          controllerAs: '$userHeader'
        },
        'navigation': {
          templateUrl: `${appConfig.tmpUrl}/analyst/analyst-navigation/analyst-navigation.html`,
          controller: 'analystNavigationCtrl',
          controllerAs: '$navigation'
        },
        'main': {
          template: '<ui-view></ui-view>',
          controller: function () {
          }
        }
      }
    })
    .state('analyst-workspace', {
      parent: 'analyst',
      url: '/workspace',
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-workspace/analyst-workspace.html`,
      controller: 'analystWorkspaceCtrl',
      controllerAs: 'vm',
      data: {pageTitle: 'Рабочее место инвестора'}
    })
    .state('analyst-entries', {
      parent: 'analyst',
      abstract: true,
      url: '/entries',
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-entries/analyst-entries-index.html`,
      controller: 'analystEntriesIndexCtrl',
      controllerAs: 'vm',
      data: {pageTitle: 'Заявки на инвестирование'}
    })
    .state('analyst-entries.entries', {
      parent: 'analyst-entries',
      url: '/',
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-entries/entries/analyst-entries.html`,
      controller: 'analystEntriesEntriesCtrl',
      controllerAs: 'vm'
    })
    .state('analyst-entries.offers', {
      parent: 'analyst-entries',
      url: '/offers',
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-entries/offers/analyst-entries-offers.html`,
      controller: 'analystEntriesOffersCtrl',
      controllerAs: 'vm'
    })
    .state('analyst-entries.consents', {
      parent: 'analyst-entries',
      url: '/consents',
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-entries/consents/analyst-entries-consents.html`,
      controller: 'analystEntriesConsentsCtrl',
      controllerAs: 'vm'
    })
    .state('analyst-entries.trash-bin', {
      parent: 'analyst-entries',
      url: '/trash-bin',
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-entries/trash-bin/analyst-entries-trash-bin.html`,
      controller: 'analystEntriesTrashBinCtrl',
      controllerAs: 'vm'
    })

    .state('analyst-loan-offer', {
      parent: 'analyst',
      url: '/loan-offer',
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-loan-offer/analyst-loan-offer.html`,
      controller: 'analystLoanOfferCtrl',
      controllerAs: 'vm',
      data: {pageTitle: 'Предложения по кредиту'}
    })
    .state('analyst-edit-reporting', {
      parent: 'analyst',
      url: '/edit-reporting',
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-edit-reporting/analyst-edit-reporting.html`,
      controller: 'analystEditReportingCtrl',
      controllerAs: 'vm',
      data: {pageTitle: 'Редактирование отчетности'}
    })
    .state('analyst-profile-settings', {
      parent: 'analyst',
      url: '/profile-settings',
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-profile-settings/analyst-profile-settings.html`,
      controller: 'analystProfileSettingsCtrl',
      controllerAs: 'vm',
      data: {pageTitle: 'Настройка профиля'}
    })
    .state('analyst-faq', {
      parent: 'analyst',
      url: '/faq',
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-faq/analyst-faq.html`,
      controller: 'analystFaqCtrl',
      controllerAs: 'vm',
      data: {pageTitle: 'Справка'}
    })

    .state('analyst-compare', {
      parent: 'analyst',
      abstract: true,
      url: '/compare',
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-compare/analyst-compare-index.html`,
      controller: 'analystCompareIndexCtrl',
      controllerAs: 'vm',
      data: {pageTitle: 'Анализ инвестиционных проектов'}
    })
    .state('analyst-compare.selection', {
      parent: 'analyst-compare',
      url: '/selection',
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-compare/selection/analyst-compare-selection.html`,
      controller: 'analystCompareSelectionCtrl',
      controllerAs: 'vm'
    })
    .state('analyst-compare.compare', {
      parent: 'analyst-compare',
      url: '/analysis',
      templateUrl: `${appConfig.tmpUrl}/analyst/analyst-compare/compare/analyst-compare-compare.html`,
      controller: 'analystCompareCompareCtrl',
      controllerAs: 'vm'
    })
}

export default config;
