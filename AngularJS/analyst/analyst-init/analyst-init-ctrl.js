'use strict';

export default  class analystInitCtrl {
  constructor($rootScope, $scope) {
    Object.assign(this, {
      $rootScope
    });
    $scope.$on('navigation:show', () => {
      this.navigationShowMode = true;
    });

    $scope.$on('background:hide', () => {
      this.navigationShowMode = false;
    });
  }

  backgroundClick() {
    if (this.navigationShowMode) {
      this.$rootScope.$broadcast('navigation:hide');
      this.navigationShowMode = false;
    }
  }
}
